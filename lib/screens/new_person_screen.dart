import 'package:cadastro_pessoas/screens/list_people_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:firebase_database/firebase_database.dart';

class NewPersonScreen extends StatefulWidget {
  @override
  NewPersonScreenState createState() {
    return new NewPersonScreenState();
  }
}

class NewPersonScreenState extends State<NewPersonScreen> {
  // Inicia a conexão com o banco, pegando o child usuários
  DatabaseReference userDatabaseReference =   FirebaseDatabase.instance.reference().child("usuarios");

  // Declarando variáveis
  DateTime selectedDate;
  DateTime dateOfnow = DateTime.now();
  String dropdownMaritalStatus;
  bool isLoading = false;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController graduateController = TextEditingController();
  TextEditingController posgraduateController = TextEditingController();
  TextEditingController courseController = TextEditingController();
  var cpfController = MaskedTextController(mask: ('000.000.000-00'));
  var cepController = MaskedTextController(mask: ('00000-000'));

  // Input de selecionar a data de nascimento
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: dateOfnow,
        firstDate: DateTime(1900, 1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
            child: Column(
              children: <Widget>[
                _title(),
                _form(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _title() {
    return Column(
      children: <Widget>[
        Text(
          'Cadastrar nova pessoa',
          style: TextStyle(fontSize: 18),
        ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: GestureDetector(
            child: Text(
              'Ou toque aqui para ver as pessoas já cadastradas',
              style: TextStyle(fontSize: 13, color: Colors.blueAccent),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ListPeopleScreen(),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  // Valida os campos do formulário. Somente os campos de formação são opcionais
  _validator(value) {
    return value.isEmpty || value == null ? "Campo obrigatório" : null;
  }

  _form() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _divider('Dados Pessoais'),
          TextFormField(
            controller: nameController,
            validator: (value) => _validator(value),
            decoration:
                InputDecoration(labelText: 'Nome', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: surnameController,
            validator: (value) => _validator(value),
            decoration: InputDecoration(
                labelText: 'Sobrenome', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: cpfController,
            keyboardType: TextInputType.number,
            validator: (value) => _validator(value),
            decoration:
                InputDecoration(labelText: 'CPF', alignLabelWithHint: true),
          ),
          _inputDate(),
          _inputMaritalStatus(),
          _divider('Endereço'),
          TextFormField(
            controller: streetController,
            validator: (value) => _validator(value),
            decoration: InputDecoration(
                labelText: 'Logradouro', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: cepController,
            keyboardType: TextInputType.number,
            validator: (value) => _validator(value),
            decoration:
                InputDecoration(labelText: 'CEP', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: cityController,
            validator: (value) => _validator(value),
            decoration:
                InputDecoration(labelText: 'Cidade', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: stateController,
            validator: (value) => _validator(value),
            decoration:
                InputDecoration(labelText: 'Estado', alignLabelWithHint: true),
          ),
          _divider('Formação'),
          TextFormField(
            controller: courseController,
            decoration:
                InputDecoration(labelText: 'Cursos', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: graduateController,
            decoration: InputDecoration(
                labelText: 'Graduação', alignLabelWithHint: true),
          ),
          TextFormField(
            controller: posgraduateController,
            decoration: InputDecoration(
                labelText: 'Pós-graduação', alignLabelWithHint: true),
          ),
          RaisedButton(
            child: isLoading
                ? SizedBox(
                    width: 16,
                    height: 16,
                    child: Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 3.0,
                      ),
                    ),
                  )
                : Text('Enviar'),
            onPressed: () {
              // Validando formulário
              if (_formKey.currentState.validate() && !isLoading) {
                setState(() {
                  isLoading = true;
                });

                // Enviando os dados do formulário para o Firebase. Com o push, ele gera um ID aleatório no banco
                userDatabaseReference.push().set(<String, String>{
                  "name": nameController.text,
                  "surname": surnameController.text,
                  "dateOfBirth": selectedDate.toIso8601String(),
                  "cpf": cpfController.text,
                  "maritalStatus": dropdownMaritalStatus,
                  "street": streetController.text,
                  "cep": cepController.text,
                  "city": cityController.text,
                  "state": stateController.text,
                  "course": courseController.text,
                  "graduate": graduateController.text,
                  "posgraduate": posgraduateController.text
                }).then((value) {

                  // Após enviar pro Firebase, limpa os campos do formulário e manda o usuário pra tela de listagem
                  setState(() {
                    nameController.clear();
                    surnameController.clear();
                    streetController.clear();
                    cityController.clear();
                    stateController.clear();
                    graduateController.clear();
                    posgraduateController.clear();
                    courseController.clear();
                    cpfController.clear();
                    cepController.clear();
                    selectedDate = null;
                    dropdownMaritalStatus = null;
                    isLoading = false;
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ListPeopleScreen(),
                    ),
                  );
                });
              }
            },
          )
        ],
      ),
    );
  }

  _inputDate() {
    return FormField<String>(validator: (_) {
      if (selectedDate == null) return "Campo obrigatório";
      return null;
    }, builder: (FormFieldState<String> state) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1, color: Colors.black45),
                  ),
                ),
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: selectedDate == null
                      ? Text(
                          'Data de Nascimento',
                          style: TextStyle(color: Colors.black54, fontSize: 16),
                        )
                      : Text(
                          selectedDate.day.toString() +
                              '/' +
                              selectedDate.month.toString() +
                              '/' +
                              selectedDate.year.toString(),
                        ),
                ),
              ),
              onTap: () {
                _selectDate(context);
              },
            ),
            Text(
              state.hasError ? state.errorText : '',
              style: TextStyle(
                color: Colors.redAccent.shade700,
                fontSize: state.hasError ? 12.0 : 0.0,
              ),
            ),
          ]);
    });
  }

  _inputMaritalStatus() {
    return FormField<String>(validator: (_) {
      if (dropdownMaritalStatus == null) return "Campo obrigatório";
      return null;
    }, builder: (FormFieldState<String> state) {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DropdownButton<String>(
              value: dropdownMaritalStatus,
              hint: Text('Estado civil'),
              underline: Container(
                height: 1,
                color: Colors.black54,
              ),
              isExpanded: true,
              onChanged: (String newValue) {
                setState(() {
                  dropdownMaritalStatus = newValue;
                });
              },
              items: <String>['Solteiro', 'Namorando', 'Casado', 'Viúvo']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
            Text(
              state.hasError ? state.errorText : '',
              style: TextStyle(
                color: Colors.redAccent.shade700,
                fontSize: state.hasError ? 12.0 : 0.0,
              ),
            ),
          ]);
    });
  }

  _divider(String text) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.blue),
        ),
      ),
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.only(top: 20, bottom: 4),
      child: Text(
        text,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
      ),
    );
  }
}
