import 'package:cadastro_pessoas/screens/list_people_screen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ViewPersonScreen extends StatefulWidget {
  String id;

  // Recebe o ID do usuário no construtor. Esse ID seja usado logo abaixo para buscar no banco de dados
  ViewPersonScreen(this.id);

  @override
  ViewPersonScreenState createState() {
    return new ViewPersonScreenState();
  }
}

class ViewPersonScreenState extends State<ViewPersonScreen> {
  // Inicia a conexão com o banco, pegando o child usuários
  DatabaseReference userDatabaseReference = FirebaseDatabase.instance.reference().child("usuarios");
  dynamic person;

  @override
  void initState() {
    // Busca o usuário pelo ID no Firebase
    userDatabaseReference.child(widget.id).once().then((DataSnapshot result) {
      if (result.value != null) {
        setState(() {
          person = result.value;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
            child: Column(
              children: <Widget>[
                _title(),
                person != null ? _info() : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _title() {
    return Column(
      children: <Widget>[
        Text(
          'Dados da pessoa',
          style: TextStyle(fontSize: 18),
        ),
        Container(
          padding: EdgeInsets.only(top: 10),
          child: GestureDetector(
            child: Text(
              'Voltar para a lista',
              style: TextStyle(fontSize: 13, color: Colors.blueAccent),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ListPeopleScreen(),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  _info() {
    DateTime dateOfBirth = DateTime.parse(person['dateOfBirth']);

    return Column(
      children: <Widget>[
        _divider('Dados Pessoais'),
        _showInfo('Nome: ', person['name']),
        _showInfo('Sobrenome: ', person['surname']),
        _showInfo('Data de Nascimento: ', dateOfBirth.day.toString() + '/' + dateOfBirth.month.toString() + '/' + dateOfBirth.year.toString()),
        _showInfo('CPF: ', person['cpf']),
        _showInfo('Estado civil: ', person['maritalStatus']),
        _divider('Endereço'),
        _showInfo('Logradouro: ', person['street']),
        _showInfo('CEP: ', person['cep']),
        _showInfo('Cidade: ', person['city']),
        _showInfo('Estado: ', person['state']),
        _divider('Formação'),
        _showInfo('Cursos: ', person['course'] != '' ? person['course'] : 'Não possui'),
        _showInfo('Graduação: ', person['graduate'] != '' ? person['graduate'] : 'Não possui'),
        _showInfo('Pós-graduação: ', person['posgraduate'] != '' ? person['posgraduate'] : 'Não possui'),
      ],
    );
  }

  _showInfo(String label, text) {
    return Row(
      children: <Widget>[
        Text(
          label,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Expanded(
          child: Text(text),
        ),
      ],
    );
  }

  _divider(String text) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.blue),
        ),
      ),
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.only(top: 20, bottom: 4),
      child: Text(
        text,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
      ),
    );
  }
}
