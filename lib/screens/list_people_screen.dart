import 'package:cadastro_pessoas/model/person.dart';
import 'package:cadastro_pessoas/screens/view_person_screen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class ListPeopleScreen extends StatefulWidget {
  @override
  ListPeopleScreenState createState() {
    return new ListPeopleScreenState();
  }
}

class ListPeopleScreenState extends State<ListPeopleScreen> {
  // Inicia a conexão com o banco, pegando o child usuários
  DatabaseReference userDatabaseReference = FirebaseDatabase.instance.reference().child("usuarios");
  TextEditingController nameController = TextEditingController();
  TextEditingController cpfController = TextEditingController();
  List<dynamic> people = [];
  List<dynamic> filteredPeople = [];
  bool isLoading = true;

  @override
  void initState() {
    // Busca todos os dados do child usuários e salva numa lista que vai conter todas as pessoas e em uma lista auxiliar que será filtrada
    userDatabaseReference.once().then((DataSnapshot result) {
      if (result.value != null) {
        result.value.forEach((index, value) {
          setState(() {
            people.add(Person(index, value));
            filteredPeople.add(Person(index, value));
          });
        });
      }
    });
    setState(() {
      isLoading = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 50),
            child: Column(
              children: <Widget>[_formSearch(), listPeople()],
            ),
          ),
        ),
      ),
    );
  }

  listPeople() {
    return isLoading
        ? CircularProgressIndicator()
        : Column(
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemCount: filteredPeople.length,
                itemBuilder: (BuildContext context, int index) {
                  // Gera um card com o nome e CPF da pessoa
                  return _cardPerson(filteredPeople[index]);
                },
              ),
            ],
          );
  }

  _cardPerson(person) {
    return GestureDetector(
      child: Card(
        elevation: 2,
        margin: EdgeInsets.symmetric(vertical: 10),
        child: Padding(
          padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
          child: Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(person.info['name']),
                  Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 5),
                    child: Text(person.info['cpf']),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      onTap: () {
        // Ao tocar no card, chama a tela de visualizar dados, passando o ID da pessoa
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ViewPersonScreen(person.id),
          ),
        );
      },
    );
  }

  _formSearch() {
    return Container(
      margin: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: nameController,
            decoration: InputDecoration(
                labelText: 'Buscar por nome', alignLabelWithHint: true),
            onChanged: (value) {
              setState(() {
                cpfController.text = '';
              });
              if (value.length == 0) {
                setState(() {
                  filteredPeople = people;
                });
              } else {
                // Verifica se existem pessoas na lista completa que tem o nome parecido com o que foi digitado na busca
                // Se houver, adiciona na lista auxiliar
                filteredPeople = [];
                people.forEach((person) {
                  if (person.info['name'].contains(nameController.text)) {
                    setState(() {
                      filteredPeople.add(person);
                    });
                  }
                });
              }
            },
          ),
          TextFormField(
            controller: cpfController,
            decoration: InputDecoration(
                labelText: 'Buscar por CPF', alignLabelWithHint: true),
            onChanged: (value) {
              setState(() {
                nameController.text = '';
              });
              if (value.length == 0) {
                setState(() {
                  filteredPeople = people;
                });
              } else {
                // Verifica se existem pessoas na lista completa que tem o CPF parecido com o que foi digitado na busca
                // Se houver, adiciona na lista auxiliar
                filteredPeople = [];
                people.forEach((person) {
                  if (person.info['cpf'].contains(cpfController.text)) {
                    setState(() {
                      filteredPeople.add(person);
                    });
                  }
                });
              }
            },
          ),
        ],
      ),
    );
  }
}
